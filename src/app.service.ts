import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'get Hello World!';
  }

  postHello(): string {
    return 'post Hello World!';
  }
}
